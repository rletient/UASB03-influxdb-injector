package org.homework.ua;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;

public class CreateDatabase {

    public static void main(String args[]) {
        final String URL = args[0];
        final String USER = args[1];
        final String PASSWORD = args[2];
        final String DBNAME = "meteo";


        InfluxDB influxDB = InfluxDBFactory.connect(URL, USER, PASSWORD);
        influxDB.setLogLevel(InfluxDB.LogLevel.FULL);
        influxDB.deleteDatabase(DBNAME);
        influxDB.createDatabase(DBNAME);
        influxDB.setDatabase(DBNAME);
        // DURATION 30 jours
        // SHARD DURATION : 3 heures
        // Nombre de replica : 1 (au delà il faut un cluster InfluxDB Enterprise
        // Retention policy par défaut de la base de données : oui
        influxDB.createRetentionPolicy("aRetentionPolicy", DBNAME, "30d", "3h", 1, true);
        influxDB.close();
    }
 }
