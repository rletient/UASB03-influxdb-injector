package org.homework.ua.model;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.IntStream;

public final class MeteoMeasure {

    private final Integer id;
    private final Integer station;
    private final LocalDateTime date;
    private final Double t;
    private final Double tn12;
    private final Double tx12;
    private final Double n;
    private final Double raf10;
    private final Double ff;
    private final Double pres;
    private final Double tend;
    private final Double rr3;
    private final Double u;
    private final Double geop;
    private final Double vv;
    private final Double htNeige;


    public MeteoMeasure(Integer id, Integer station, LocalDateTime date, Double t, Double tn12, Double tx12, Double n, Double raf10, Double ff, Double pres, Double tend, Double rr3, Double u, Double geop, Double vv, Double htNeige) {
        this.id = id;
        this.station = station;
        this.date = date;
        this.t = t;
        this.tn12 = tn12;
        this.tx12 = tx12;
        this.n = n;
        this.raf10 = raf10;
        this.ff = ff;
        this.pres = pres;
        this.tend = tend;
        this.rr3 = rr3;
        this.u = u;
        this.geop = geop;
        this.vv = vv;
        this.htNeige = htNeige;
    }

    public Integer getId() {
        return id;
    }

    public Integer getStation() {
        return station;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Double getT() {
        return t;
    }

    public Double getTn12() {
        return tn12;
    }

    public Double getTx12() {
        return tx12;
    }

    public Double getN() {
        return n;
    }

    public Double getRaf10() {
        return raf10;
    }

    public Double getFf() {
        return ff;
    }

    public Double getPres() {
        return pres;
    }

    public Double getTend() {
        return tend;
    }

    public Double getRr3() {
        return rr3;
    }

    public Double getU() {
        return u;
    }

    public Double getGeop() {
        return geop;
    }

    public Double getVv() {
        return vv;
    }

    public Double getHtNeige() {
        return htNeige;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MeteoMeasure that = (MeteoMeasure) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "MeteoMeasure{" +
                "id=" + id +
                ", station=" + station +
                ", date=" + date +
                ", t=" + t +
                ", tn12=" + tn12 +
                ", tx12=" + tx12 +
                ", n=" + n +
                ", raf10=" + raf10 +
                ", ff=" + ff +
                ", pres=" + pres +
                ", tend=" + tend +
                ", rr3=" + rr3 +
                ", u=" + u +
                ", geop=" + geop +
                ", vv=" + vv +
                ", htNeige=" + htNeige +
                '}';
    }
}

