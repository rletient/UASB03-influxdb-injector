package org.homework.ua.model.builder;

import org.homework.ua.model.MeteoMeasure;

import java.time.LocalDateTime;

public class MeteoMeasureBuilder {
    private Integer id;
    private Integer station;
    private LocalDateTime date;
    private Double t;
    private Double tn12;
    private Double tx12;
    private Double n;
    private Double raf10;
    private Double ff;
    private Double pres;
    private Double tend;
    private Double rr3;
    private Double u;
    private Double geop;
    private Double vv;
    private Double htNeige;

    public MeteoMeasureBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public MeteoMeasureBuilder setStation(Integer station) {
        this.station = station;
        return this;
    }

    public MeteoMeasureBuilder setDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public MeteoMeasureBuilder setT(Double t) {
        this.t = t;
        return this;
    }

    public MeteoMeasureBuilder setTn12(Double tn12) {
        this.tn12 = tn12;
        return this;
    }

    public MeteoMeasureBuilder setTx12(Double tx12) {
        this.tx12 = tx12;
        return this;
    }

    public MeteoMeasureBuilder setN(Double n) {
        this.n = n;
        return this;
    }

    public MeteoMeasureBuilder setRaf10(Double raf10) {
        this.raf10 = raf10;
        return this;
    }

    public MeteoMeasureBuilder setFf(Double ff) {
        this.ff = ff;
        return this;
    }

    public MeteoMeasureBuilder setPres(Double pres) {
        this.pres = pres;
        return this;
    }

    public MeteoMeasureBuilder setTend(Double tend) {
        this.tend = tend;
        return this;
    }

    public MeteoMeasureBuilder setRr3(Double rr3) {
        this.rr3 = rr3;
        return this;
    }

    public MeteoMeasureBuilder setU(Double u) {
        this.u = u;
        return this;
    }

    public MeteoMeasureBuilder setGeop(Double geop) {
        this.geop = geop;
        return this;
    }

    public MeteoMeasureBuilder setVv(Double vv) {
        this.vv = vv;
        return this;
    }

    public MeteoMeasureBuilder setHtNeige(Double htNeige) {
        this.htNeige = htNeige;
        return this;
    }

    public MeteoMeasure createMeteoMeasure() {
        return new MeteoMeasure(id, station, date, t, tn12, tx12, n, raf10, ff, pres, tend, rr3, u, geop, vv, htNeige);
    }
}