package org.homework.ua.csv;

import org.homework.ua.model.MeteoMeasure;
import org.homework.ua.model.builder.MeteoMeasureBuilder;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Function;

public class MeteoMeasureBuilderCSV {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private static final Function<String, Integer> F_INTEGER = (token -> Integer.parseInt(token));
    private static final Function<String, Double> F_DOUBLE = (token -> Double.parseDouble(token));
    private static final Function<String, LocalDateTime> F_DATE = (token -> LocalDateTime.parse(token, formatter));

    public static MeteoMeasure build(String line) {
        final MeteoMeasureBuilder builder = new MeteoMeasureBuilder();
        String tokens[] = line.split(",");
        builder.setId(F_INTEGER.apply(tokens[0]));
        builder.setStation(F_INTEGER.apply(tokens[1]));
        builder.setDate(F_DATE.apply(tokens[2]));

        builder.setT(F_DOUBLE.apply(tokens[3]));
        builder.setTn12(F_DOUBLE.apply(tokens[4]));
        builder.setTx12(F_DOUBLE.apply(tokens[5]));
        builder.setN(F_DOUBLE.apply(tokens[6]));
        builder.setRaf10(F_DOUBLE.apply(tokens[7]));
        builder.setFf(F_DOUBLE.apply(tokens[8]));
        builder.setPres(F_DOUBLE.apply(tokens[9]));
        builder.setTend(F_DOUBLE.apply(tokens[10]));
        builder.setRr3(F_DOUBLE.apply(tokens[11]));
        builder.setU(F_DOUBLE.apply(tokens[12]));
        builder.setGeop(F_DOUBLE.apply(tokens[13]));
        builder.setVv(F_DOUBLE.apply(tokens[14]));
        builder.setHtNeige(F_DOUBLE.apply(tokens[15]));

        return builder.createMeteoMeasure();
    }
}
