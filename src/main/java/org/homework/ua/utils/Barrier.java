package org.homework.ua.utils;

import org.homework.ua.model.MeteoMeasure;

import java.time.LocalDateTime;

public class Barrier {

    private LocalDateTime barrierTime = null;

    private long time;

    public Barrier(long time) {
        this.time = time;
    }

    public void pauseIfNecessary(MeteoMeasure meteoMeasure) {
        if(barrierTime == null) {
            barrierTime = meteoMeasure.getDate();
        }
        if(!barrierTime.equals(meteoMeasure.getDate())) {
            barrierTime = null;
            try {
                // 1 Minute de pause
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
