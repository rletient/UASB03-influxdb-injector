package org.homework.ua;

import org.homework.ua.csv.MeteoMeasureBuilderCSV;
import org.homework.ua.utils.Barrier;
import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class Injector {

    public static void main(String[] args) {
        final String URL = args[0];
        final String USER = args[1];
        final String PASSWORD = args[2];
        final String DBNAME = "meteo";

        final String FILE = args[3];

        final InfluxDB influxDB = InfluxDBFactory.connect(URL, USER, PASSWORD);
        influxDB.setDatabase(DBNAME);
        influxDB.setRetentionPolicy("aRetentionPolicy");
        // Flush every 2000 Points, at least every 100ms
        influxDB.enableBatch(BatchOptions.DEFAULTS.actions(2000).flushDuration(100));

        try {
            final Barrier barrier = new Barrier(1000 * 60);
            Files.lines(Paths.get(FILE))
                    .skip(1)
                    .map(MeteoMeasureBuilderCSV::build)
                    .peek(barrier::pauseIfNecessary)
                    .peek(System.out::println)
                    .forEach(meteoMeasure ->
                            influxDB.write(
                                    Point.measurement("meteo_measure").time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                                            .addField("id", meteoMeasure.getId())
                                            .addField("station", meteoMeasure.getStation())
                                            .addField("t", meteoMeasure.getT())
                                            .addField("tn12", meteoMeasure.getTn12())
                                            .addField("tx12", meteoMeasure.getTx12())
                                            .addField("n", meteoMeasure.getN())
                                            .addField("raf10", meteoMeasure.getRaf10())
                                            .addField("ff", meteoMeasure.getFf())
                                            .addField("pres", meteoMeasure.getPres())
                                            .addField("tend", meteoMeasure.getTend())
                                            .addField("rr3", meteoMeasure.getRr3())
                                            .addField("u", meteoMeasure.getU())
                                            .addField("geop", meteoMeasure.getGeop())
                                            .addField("vv", meteoMeasure.getVv())
                                            .addField("htNeige", meteoMeasure.getHtNeige())
                                            .build())
                    );
        } catch (IOException e) {
            e.printStackTrace();
        }
        influxDB.close();
    }
}
